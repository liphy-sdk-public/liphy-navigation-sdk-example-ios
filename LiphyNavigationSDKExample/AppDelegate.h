//
//  AppDelegate.h
//  LiphyNavigationSDKExample
//
//  Created by Developer on 29/6/2020.
//  Copyright © 2020 liphy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

