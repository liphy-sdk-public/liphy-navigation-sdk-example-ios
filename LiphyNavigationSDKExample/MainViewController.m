//
//  ViewController.m
//  LiphyNavigationliphy SDKExample
//
//  Created by Developer on 29/6/2020.
//  Copyright © 2020 liphy. All rights reserved.
//

#import "MainViewController.h"
#import <LiphyNavigationSDK/LiphyNavigationSDK.h>
#import <Parse/Parse.h>

@interface MainViewController () <LFLocationDelegate, LFBluetoothDeviceDelegate>

@property (nonatomic) LFLocationManager *locationManager;

// location info provided by LiPHY Navigation SDK
@property (weak, nonatomic) IBOutlet UILabel *latitude;
@property (weak, nonatomic) IBOutlet UILabel *longitude;
@property (weak, nonatomic) IBOutlet UILabel *floor;
@property (weak, nonatomic) IBOutlet UILabel *buildingId;
@property (weak, nonatomic) IBOutlet UILabel *buildingName;
@property (weak, nonatomic) IBOutlet UILabel *provider;

// bluetooth
@property (weak, nonatomic) IBOutlet UILabel *bluetoothName;

// LiPHY Scanner SDK info
@property (weak, nonatomic) IBOutlet UILabel *version;
@property (weak, nonatomic) IBOutlet UILabel *expiryDate;

@property (weak, nonatomic) IBOutlet UILabel *cameraSide;

@property (weak, nonatomic) IBOutlet UILabel *username;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Initialize the location manager
    self.locationManager = [[LFLocationManager alloc] init];
    self.locationManager.locationDelegate = self;
	self.locationManager.bluetoothDeviceDelegate = self;
    
    // Show the SDK Version number and Expiry Date
    _version.text = [[NSString alloc] initWithFormat:@"SDK Version: %@", _locationManager.lightManager.sdkVersion];
    _expiryDate.text = [[NSString alloc] initWithFormat:@"Expiry Date: %@", [LFLocationManager getExpiryDate]];
    
    // Start Bluetooth Scanning
    [self.locationManager startBluetoothScan];
    
    
	// logout before login to remove memory
	[self.locationManager.cloudManager logout];
	self.username.text = @"User";

	/*
	 test account for logging in to the cloud
	 username: example
	 password: 123456
	 */
    
    // Do the login and show the username of the logged in user
	[self.locationManager.cloudManager loginWithUsername:@"example" password:@"123456" completion:^(PFUser * _Nullable user, NSError* _Nullable error) {
		if (error == nil) {
			self.username.text = [[NSString alloc] initWithFormat:@"Username: %@", user.username];
		} else {
			NSLog([[NSString alloc] initWithFormat:@"Login error, code:%ld", (long)error.code]);
			NSLog(@"%@", error.localizedDescription);

			self.username.text = [[NSString alloc] initWithFormat:@"Login error: %@", error.localizedDescription];
		}
	}];
    
}

// The callback for any Bluetooth device received
- (void)onBluetoothInfoReadyWithDevice:(BluetoothDevice *)device {
	_bluetoothName.text = [[NSString alloc] initWithFormat:@"Latest bluetooth received: %@", device.name];
}

// The callback for Location Update of the user based on LiPHY, PDR, or Bluetooth
- (void)onLocationUpdateWithLocation:(IndoorLocation *)location {
    NSLog(@"liphy sdk location: %f, %f", location.latitude, location.longitude);
    
    _latitude.text = [[NSString alloc] initWithFormat:@"Latitude: %f", location.latitude];
    _longitude.text = [[NSString alloc] initWithFormat:@"Longitude: %f", location.longitude];
    _floor.text = [[NSString alloc] initWithFormat:@"Floor: %@", location.floor];
    _buildingId.text = [[NSString alloc] initWithFormat:@"Building ID: %@", location.buildingId];
    _buildingName.text = [[NSString alloc] initWithFormat:@"Building Name: %@", location.buildingName];

    NSString *providerString;
    switch (location.provider) {
        case LocationProviderLiphy:
            providerString = @"Provider: LIPHY";
            break;
        case LocationProviderPdr:
            providerString = @"Provider: PDR";
            break;
        case LocationProviderBluetooth:
            providerString = @"Provider: BLUETOOTH";
            break;
        default:
            providerString = @"Bug. Please report to us.";
            break;
    }
    _provider.text = providerString;
}

// Perform switching of the camera
- (IBAction)switchCamera:(id)sender {
    [self.locationManager switchCamera:!self.locationManager.lightManager.isFront];
    
    if (self.locationManager.lightManager.isFront) {
        _cameraSide.text = @"Using Front Camera";
    } else {
        _cameraSide.text = @"Using Back Camera";
    }
}

@end
