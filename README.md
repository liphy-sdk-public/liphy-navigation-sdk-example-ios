# LiPHY Navigation SDK for iOS


## Introduction

- LiPHY Navigation SDK is based on combined usage of LiPHY, PDR and Bluetooth algorithms for indoor navigation. The SDK provides your app with live location udpates of the user via LFLocationManager. The location update callback contains an IndoorLocation object that provides precise location information of the user. The IndoorLocation object contains longitude, latitude, building, and floor information. In addition, IndoorLocation object also specifies the type of location provider i.e., whether the location is provided via Bluetooth, PDR, or LiPHY (VLC). This information can be directly passed to a 3rd party digital map API to show the location of the user on a map or it can be used to trigger location related events in your app.

## Download and Install LiPHY Navigation SDK

- Following steps will tell you how to download the SDK and add it to your project directory and download related dependencies.

### 1. Extract the SDK

- (Contact `info@liphy.io` for the SDK download link)
- Download the SDK from the provided link. It should contain two folders, `LightFlySDK` and `LiphyNavigationSDK`.
- Move your `LightFlySDK` and `LiphyNavigationSDK` folder to your project root


    ![libs directory](File structure.png)

### 2. Add dependencies

- `Podfile`

    ```xml
    target 'YourAppName' do
      # Comment the next line if you don't want to use dynamic frameworks
      use_frameworks!

      # Pods for YourAppName
      pod 'Parse'
      pod 'LightFlySDK', :path => './LightFlySDK'
      pod 'LiphyNavigationSDK', :path => ‘./LiphyNavigationSDK‘

        ...

    end
    ```
- change directory to your root project folder and run "pod update" in your terminal 

## Configure your iOS App

- Once you have successfully setup the SDK and downloaded the dependencies, you are ready to open your project workspace in Xcode and start using the SDK.
- Note that below steps will guide you on the minimum settings you need to configure in your project to use the SDK. For this example app, these steps are already performed, you just need to compile and run the app in Xcode.

### 3. Build Settings

- Open project workspace in Xcode

- Change your build setting for `Allow Non-modualr Includes In Framework Module` to `YES`

    ![libs directory](Build setting.png)

### 4. Set up permissions and register cloud service

1. **Info.plist**

    ![libs directory](Info.plist.png)

    - Apply the above settings to your `Info.plist`
    - source code

        ```xml
        <key>NSMotionUsageDescription</key>
        <string>You will need motion sensor to determine your location</string>
        <key>NSBluetoothAlwaysUsageDescription</key>
        <string>Allow Bluetooth access for LiPHY</string>
        <key>NSLocationAlwaysUsageDescription</key>
        <string>You can recieve information while you are nearby LiPHY enabled signgae and displays</string>
        <key>NSLocationUsageDescription</key>
        <string>You can recieve information while you are nearby LiPHY enabled signgae and displays</string>
        <key>NSLocationWhenInUseUsageDescription</key>
        <string>You can recieve information while you are nearby LiPHY enabled signgae and displays</string>
        <key>NSCameraUsageDescription</key>
        <string>You will need to point the camera at LiPHY-enabled light sources to capture images and extract information from them. No pictures will be taken and stored.</string>
        <key>NSPhotoLibraryUsageDescription</key>
        <string>Allow photo access for LiPHY management</string>
        ```

2. **AppDelegate.m**

    ```objc
    - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
            
        [LFLocationManager initParse];
        
        return YES;
    }
    ```

### 5. ViewController

```objc
#import "ViewController.h"
#import <LiphyNavigationSDK/LiphyNavigationSDK.h>

@interface ViewController () <LFLocationDelegate>

@property (nonatomic) LFLocationManager *locationManager;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.locationManager = [[LFLocationManager alloc] init];
    self.locationManager.locationDelegate = self;
    
	[self.locationManager startBluetoothScan];
	
}

- (void)onLocationUpdateWithLocation:(IndoorLocation *)location {
        // receive location callback from LiPHY/PDR/Bluetooth here
        // "IndoorLocation" object contains : Longitude, Latitude, Building, and Floor Information
}

@end
```



